set moduleName top_func
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {top_func}
set C_modelType { void 0 }
set C_modelArgList {
	{ inVec1_V int 16 regular {array 100 { 1 3 } 1 1 }  }
	{ inVec2_V int 16 regular {array 100 { 1 3 } 1 1 }  }
	{ outVecSum_V int 16 regular {array 100 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "inVec1_V", "interface" : "memory", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "inVec1.V","cData": "int16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 99,"step" : 1}]}]}]} , 
 	{ "Name" : "inVec2_V", "interface" : "memory", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "inVec2.V","cData": "int16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 99,"step" : 1}]}]}]} , 
 	{ "Name" : "outVecSum_V", "interface" : "memory", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "outVecSum.V","cData": "int16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 99,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ inVec1_V_address0 sc_out sc_lv 7 signal 0 } 
	{ inVec1_V_ce0 sc_out sc_logic 1 signal 0 } 
	{ inVec1_V_q0 sc_in sc_lv 16 signal 0 } 
	{ inVec2_V_address0 sc_out sc_lv 7 signal 1 } 
	{ inVec2_V_ce0 sc_out sc_logic 1 signal 1 } 
	{ inVec2_V_q0 sc_in sc_lv 16 signal 1 } 
	{ outVecSum_V_address0 sc_out sc_lv 7 signal 2 } 
	{ outVecSum_V_ce0 sc_out sc_logic 1 signal 2 } 
	{ outVecSum_V_we0 sc_out sc_logic 1 signal 2 } 
	{ outVecSum_V_d0 sc_out sc_lv 16 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "inVec1_V_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "inVec1_V", "role": "address0" }} , 
 	{ "name": "inVec1_V_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "inVec1_V", "role": "ce0" }} , 
 	{ "name": "inVec1_V_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "inVec1_V", "role": "q0" }} , 
 	{ "name": "inVec2_V_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "inVec2_V", "role": "address0" }} , 
 	{ "name": "inVec2_V_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "inVec2_V", "role": "ce0" }} , 
 	{ "name": "inVec2_V_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "inVec2_V", "role": "q0" }} , 
 	{ "name": "outVecSum_V_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "outVecSum_V", "role": "address0" }} , 
 	{ "name": "outVecSum_V_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "outVecSum_V", "role": "ce0" }} , 
 	{ "name": "outVecSum_V_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "outVecSum_V", "role": "we0" }} , 
 	{ "name": "outVecSum_V_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "outVecSum_V", "role": "d0" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4"],
		"CDFG" : "top_func",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "0", "ap_idle" : "1",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "1005", "EstimateLatencyMax" : "1005",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "inVec1_V", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "inVec2_V", "Type" : "Memory", "Direction" : "I"},
			{"Name" : "outVecSum_V", "Type" : "Memory", "Direction" : "O"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.vec1_data_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.vec2_data_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.resVec_data_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.result_data_V_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	top_func {
		inVec1_V {Type I LastRead 1 FirstWrite -1}
		inVec2_V {Type I LastRead 2 FirstWrite -1}
		outVecSum_V {Type O LastRead -1 FirstWrite 6}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1005", "Max" : "1005"}
	, {"Name" : "Interval", "Min" : "1006", "Max" : "1006"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	inVec1_V { ap_memory {  { inVec1_V_address0 mem_address 1 7 }  { inVec1_V_ce0 mem_ce 1 1 }  { inVec1_V_q0 mem_dout 0 16 } } }
	inVec2_V { ap_memory {  { inVec2_V_address0 mem_address 1 7 }  { inVec2_V_ce0 mem_ce 1 1 }  { inVec2_V_q0 mem_dout 0 16 } } }
	outVecSum_V { ap_memory {  { outVecSum_V_address0 mem_address 1 7 }  { outVecSum_V_ce0 mem_ce 1 1 }  { outVecSum_V_we0 mem_we 1 1 }  { outVecSum_V_d0 mem_din 1 16 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
