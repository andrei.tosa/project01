// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#define AP_INT_MAX_W 32678

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;


// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->


// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "inVec1_V"
#define AUTOTB_TVIN_inVec1_V  "../tv/cdatafile/c.top_func.autotvin_inVec1_V.dat"
// wrapc file define: "inVec2_V"
#define AUTOTB_TVIN_inVec2_V  "../tv/cdatafile/c.top_func.autotvin_inVec2_V.dat"
// wrapc file define: "outVecSum_V"
#define AUTOTB_TVOUT_outVecSum_V  "../tv/cdatafile/c.top_func.autotvout_outVecSum_V.dat"
#define AUTOTB_TVIN_outVecSum_V  "../tv/cdatafile/c.top_func.autotvin_outVecSum_V.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "outVecSum_V"
#define AUTOTB_TVOUT_PC_outVecSum_V  "../tv/rtldatafile/rtl.top_func.autotvout_outVecSum_V.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			inVec1_V_depth = 0;
			inVec2_V_depth = 0;
			outVecSum_V_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{inVec1_V " << inVec1_V_depth << "}\n";
			total_list << "{inVec2_V " << inVec2_V_depth << "}\n";
			total_list << "{outVecSum_V " << outVecSum_V_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int inVec1_V_depth;
		int inVec2_V_depth;
		int outVecSum_V_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};

extern void top_func (
const ap_int<16> inVec1[100],
const ap_int<16> inVec2[100],
ap_int<16> outVecSum[100]);

void AESL_WRAP_top_func (
const ap_int<16> inVec1[100],
const ap_int<16> inVec2[100],
ap_int<16> outVecSum[100])
{
	refine_signal_handler();
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		CodeState = ENTER_WRAPC_PC;
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;


		// output port post check: "outVecSum_V"
		aesl_fh.read(AUTOTB_TVOUT_PC_outVecSum_V, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_outVecSum_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_outVecSum_V, AESL_token); // data

			sc_bv<16> *outVecSum_V_pc_buffer = new sc_bv<16>[100];
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'outVecSum_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'outVecSum_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					outVecSum_V_pc_buffer[i] = AESL_token.c_str();
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_outVecSum_V, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_outVecSum_V))
				{
					exit(1);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: outVecSum_V
				{
					// bitslice(15, 0)
					// {
						// celement: outVecSum.V(15, 0)
						// {
							sc_lv<16>* outVecSum_V_lv0_0_99_1 = new sc_lv<16>[100];
						// }
					// }

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: outVecSum.V(15, 0)
						{
							// carray: (0) => (99) @ (1)
							for (int i_0 = 0; i_0 <= 99; i_0 += 1)
							{
								if (&(outVecSum[0]) != NULL) // check the null address if the c port is array or others
								{
									outVecSum_V_lv0_0_99_1[hls_map_index].range(15, 0) = sc_bv<16>(outVecSum_V_pc_buffer[hls_map_index].range(15, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: outVecSum.V(15, 0)
						{
							// carray: (0) => (99) @ (1)
							for (int i_0 = 0; i_0 <= 99; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : outVecSum[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : outVecSum[0]
								// output_left_conversion : outVecSum[i_0]
								// output_type_conversion : (outVecSum_V_lv0_0_99_1[hls_map_index]).to_string(SC_BIN).c_str()
								if (&(outVecSum[0]) != NULL) // check the null address if the c port is array or others
								{
									outVecSum[i_0] = (outVecSum_V_lv0_0_99_1[hls_map_index]).to_string(SC_BIN).c_str();
									hls_map_index++;
								}
							}
						}
					}
				}
			}

			// release memory allocation
			delete [] outVecSum_V_pc_buffer;
		}

		AESL_transaction_pc++;
	}
	else
	{
		CodeState = ENTER_WRAPC;
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "inVec1_V"
		char* tvin_inVec1_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_inVec1_V);

		// "inVec2_V"
		char* tvin_inVec2_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_inVec2_V);

		// "outVecSum_V"
		char* tvin_outVecSum_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_outVecSum_V);
		char* tvout_outVecSum_V = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_outVecSum_V);

		CodeState = DUMP_INPUTS;
		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// [[transaction]]
		sprintf(tvin_inVec1_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_inVec1_V, tvin_inVec1_V);

		sc_bv<16>* inVec1_V_tvin_wrapc_buffer = new sc_bv<16>[100];

		// RTL Name: inVec1_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: inVec1.V(15, 0)
				{
					// carray: (0) => (99) @ (1)
					for (int i_0 = 0; i_0 <= 99; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : inVec1[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : inVec1[0]
						// regulate_c_name       : inVec1_V
						// input_type_conversion : (inVec1[i_0]).to_string(2).c_str()
						if (&(inVec1[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> inVec1_V_tmp_mem;
							inVec1_V_tmp_mem = (inVec1[i_0]).to_string(2).c_str();
							inVec1_V_tvin_wrapc_buffer[hls_map_index].range(15, 0) = inVec1_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 100; i++)
		{
			sprintf(tvin_inVec1_V, "%s\n", (inVec1_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_inVec1_V, tvin_inVec1_V);
		}

		tcl_file.set_num(100, &tcl_file.inVec1_V_depth);
		sprintf(tvin_inVec1_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_inVec1_V, tvin_inVec1_V);

		// release memory allocation
		delete [] inVec1_V_tvin_wrapc_buffer;

		// [[transaction]]
		sprintf(tvin_inVec2_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_inVec2_V, tvin_inVec2_V);

		sc_bv<16>* inVec2_V_tvin_wrapc_buffer = new sc_bv<16>[100];

		// RTL Name: inVec2_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: inVec2.V(15, 0)
				{
					// carray: (0) => (99) @ (1)
					for (int i_0 = 0; i_0 <= 99; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : inVec2[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : inVec2[0]
						// regulate_c_name       : inVec2_V
						// input_type_conversion : (inVec2[i_0]).to_string(2).c_str()
						if (&(inVec2[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> inVec2_V_tmp_mem;
							inVec2_V_tmp_mem = (inVec2[i_0]).to_string(2).c_str();
							inVec2_V_tvin_wrapc_buffer[hls_map_index].range(15, 0) = inVec2_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 100; i++)
		{
			sprintf(tvin_inVec2_V, "%s\n", (inVec2_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_inVec2_V, tvin_inVec2_V);
		}

		tcl_file.set_num(100, &tcl_file.inVec2_V_depth);
		sprintf(tvin_inVec2_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_inVec2_V, tvin_inVec2_V);

		// release memory allocation
		delete [] inVec2_V_tvin_wrapc_buffer;

		// [[transaction]]
		sprintf(tvin_outVecSum_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_outVecSum_V, tvin_outVecSum_V);

		sc_bv<16>* outVecSum_V_tvin_wrapc_buffer = new sc_bv<16>[100];

		// RTL Name: outVecSum_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: outVecSum.V(15, 0)
				{
					// carray: (0) => (99) @ (1)
					for (int i_0 = 0; i_0 <= 99; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : outVecSum[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : outVecSum[0]
						// regulate_c_name       : outVecSum_V
						// input_type_conversion : (outVecSum[i_0]).to_string(2).c_str()
						if (&(outVecSum[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> outVecSum_V_tmp_mem;
							outVecSum_V_tmp_mem = (outVecSum[i_0]).to_string(2).c_str();
							outVecSum_V_tvin_wrapc_buffer[hls_map_index].range(15, 0) = outVecSum_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 100; i++)
		{
			sprintf(tvin_outVecSum_V, "%s\n", (outVecSum_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_outVecSum_V, tvin_outVecSum_V);
		}

		tcl_file.set_num(100, &tcl_file.outVecSum_V_depth);
		sprintf(tvin_outVecSum_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_outVecSum_V, tvin_outVecSum_V);

		// release memory allocation
		delete [] outVecSum_V_tvin_wrapc_buffer;

// [call_c_dut] ---------->

		CodeState = CALL_C_DUT;
		top_func(inVec1, inVec2, outVecSum);

		CodeState = DUMP_OUTPUTS;

		// [[transaction]]
		sprintf(tvout_outVecSum_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_outVecSum_V, tvout_outVecSum_V);

		sc_bv<16>* outVecSum_V_tvout_wrapc_buffer = new sc_bv<16>[100];

		// RTL Name: outVecSum_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: outVecSum.V(15, 0)
				{
					// carray: (0) => (99) @ (1)
					for (int i_0 = 0; i_0 <= 99; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : outVecSum[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : outVecSum[0]
						// regulate_c_name       : outVecSum_V
						// input_type_conversion : (outVecSum[i_0]).to_string(2).c_str()
						if (&(outVecSum[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> outVecSum_V_tmp_mem;
							outVecSum_V_tmp_mem = (outVecSum[i_0]).to_string(2).c_str();
							outVecSum_V_tvout_wrapc_buffer[hls_map_index].range(15, 0) = outVecSum_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < 100; i++)
		{
			sprintf(tvout_outVecSum_V, "%s\n", (outVecSum_V_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_outVecSum_V, tvout_outVecSum_V);
		}

		tcl_file.set_num(100, &tcl_file.outVecSum_V_depth);
		sprintf(tvout_outVecSum_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_outVecSum_V, tvout_outVecSum_V);

		// release memory allocation
		delete [] outVecSum_V_tvout_wrapc_buffer;

		CodeState = DELETE_CHAR_BUFFERS;
		// release memory allocation: "inVec1_V"
		delete [] tvin_inVec1_V;
		// release memory allocation: "inVec2_V"
		delete [] tvin_inVec2_V;
		// release memory allocation: "outVecSum_V"
		delete [] tvout_outVecSum_V;
		delete [] tvin_outVecSum_V;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
	}
}

