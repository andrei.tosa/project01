// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern void AESL_WRAP_top_func (
const ap_int<16> inVec1[100],
const ap_int<16> inVec2[100],
ap_int<16> outVecSum[100]);
