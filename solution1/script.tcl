############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project Project01
set_top top_func
add_files Project01/src/main_component.cpp
add_files Project01/src/main_component.h
add_files Project01/src/vector.h
add_files -tb Project01/src/main_tb.cpp
open_solution "solution1"
set_part {xcu250-figd2104-2L-e}
create_clock -period 10 -name default
#source "./Project01/solution1/directives.tcl"
csim_design
csynth_design
cosim_design -rtl vhdl
export_design -format ip_catalog
