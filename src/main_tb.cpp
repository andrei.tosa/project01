#include <iostream>
#include <assert.h>
#include "main_component.h"

using namespace std;
using namespace hls;

int main(int argc, char const *argv[]) {
	CVector arr1, arr2, arr3;

	for (int i = 0; i < ARRAY_SIZE; i++) {
		arr1[i] = 0;
		arr2[i] = 0;
	}

	top_func(arr1, arr2, arr3);

	for (int i = 0; i < ARRAY_SIZE; i++)
		assert(arr3[i] == 0);


	for (int i = 0; i < ARRAY_SIZE; i++) {
			arr1[i] = 5;
			arr2[i] = i;
	}

	top_func(arr1, arr2, arr3);

	for (int i = 0; i < ARRAY_SIZE; i++)
		assert(arr3[i] == 5 + i);

	cout << "All tests passed!\n";
}
