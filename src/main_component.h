#pragma once
#include "vector.h"
#include <ap_int.h>

#define ARRAY_SIZE 100
#define WORD_SIZE 16
typedef hls::Vector<ap_int<WORD_SIZE>, ARRAY_SIZE> CPPVector;
typedef ap_int<WORD_SIZE> CVector[ARRAY_SIZE];

void top_func(
		const CVector inVec1,
        const CVector inVec2,
		CVector outVectorSum);
