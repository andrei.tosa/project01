#pragma once
namespace hls {

	template <class TScalar, unsigned int length>
	class Vector{
	private:
		TScalar _data[length];
	public:
		Vector();
		Vector(const TScalar init[length]);
		Vector<TScalar, length> operator+ (const Vector<TScalar, length> & b) const;
		void copyData(TScalar vec[length]) const;
	};


	template <class TScalar, unsigned int length>
	Vector<TScalar, length>::Vector() {}

	template <class TScalar, unsigned int length>
	Vector<TScalar, length>::Vector(const TScalar init[length]) {
		for (unsigned int i = 0; i < length; i++) {
			this->_data[i] = init[i];
		}
	}

	template <class TScalar, unsigned int length>
	Vector<TScalar, length> Vector<TScalar, length>::operator+(const Vector<TScalar, length> & b) const {
		Vector<TScalar, length> result;
		for (unsigned int i = 0; i < length; i++) {
			result._data[i] = this->_data[i] + b._data[i];
		}

		return result;
	}

	template <class TScalar, unsigned int length>
	void Vector<TScalar, length>::copyData(TScalar dst[length]) const {
		for (int i = 0; i < length; i++) {
			dst[i] = this->_data[i];
		}
	}

}
