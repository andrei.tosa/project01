
#ifndef __SYNTHESIS__
#include <iostream>
#endif

#include "main_component.h"

void top_func(
		const CVector inVec1,
        const CVector inVec2,
		CVector outVecSum)
{
	CPPVector vec1(inVec1), vec2(inVec2), resVec;

	resVec = vec1 + vec2;
	resVec.copyData(outVecSum);

#ifndef __SYNTHESIS__
	std::cout << "Addition finished\n";
#endif

}
